package api.maps;

import android.view.View;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by dhimil on 7/12/15.
 */
public interface MapEvents {
    public boolean markerClick(Marker marker);
    public View getInfoWindow(Marker marker);
    public View getInfoContents(Marker marker);
}
