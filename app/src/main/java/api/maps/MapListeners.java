package api.maps;

import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by dhimil on 7/12/15.
 */
public class MapListeners implements
        GoogleMap.OnMarkerClickListener, GoogleMap.InfoWindowAdapter {
    MapEvents events;
    public MapListeners(MapEvents events) {
        this.events = events;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return events.markerClick(marker);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return events.getInfoWindow(marker);
    }

    @Override
    public View getInfoContents(Marker marker) {
        return events.getInfoContents(marker);
    }
}
