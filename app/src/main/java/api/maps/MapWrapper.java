package api.maps;

import android.support.v4.app.FragmentActivity;

import com.dgos.findfood.findfood.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by dhimil on 7/12/15.
 */
public class MapWrapper {
    private final GoogleMap map;

    public MapWrapper(FragmentActivity mapsActivity) {
        this.map = ((SupportMapFragment) mapsActivity
                .getSupportFragmentManager()
                .findFragmentById(R.id.map))
                .getMap();
        if (this.map != null) {
            defaultMapSettings();
        }
    }

    public MapWrapper(FragmentActivity fragmentActivity, MapEvents events) {
        this(fragmentActivity);
        if (events != null) {
            MapListeners listeners = new MapListeners(events);
            this.map.setOnMarkerClickListener(listeners);
            this.map.setInfoWindowAdapter(listeners);
        }
    }

    public void defaultMapSettings() {
//        map.getUiSettings().setZoomGesturesEnabled(false);
//        map.setMyLocationEnabled(true);
//        Location location = map.getMyLocation();
//        map.setMyLocationEnabled(true);
        // Dummy location in SF.. Too lazy code..
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(37.782, -122.447), 14));
    }

    public void addMarker(String title, double lattitude, double longitude) {
        this.addMarker(title, lattitude, longitude, BitmapDescriptorFactory.HUE_GREEN);
    }

    public void addMarker(String title, double lattitude, double longitude, float markerType) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(markerType))
                .anchor(0.0f, 1.0f)
                .position(new LatLng(lattitude, longitude))
                .title(title);
        map.addMarker(markerOptions);
    }
}
