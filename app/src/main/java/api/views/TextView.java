package api.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;

/**
 * Created by dhimil on 7/12/15.
 */
public class TextView extends android.widget.TextView {

    private String viewText;

    public TextView(Context context) {
        super(context);
    }

    public TextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public TextView(Context context, String viewText) {
        super(context);
        this.viewText = viewText;
        setDefaults();
    }

    public void setDefaults() {
        this.setText(viewText);
        this.setTextColor(Color.BLACK);
        this.setLineSpacing(1f, 1f);
        this.setBackgroundColor(Color.WHITE);
        this.setPadding(25, 25, 25, 25);
    }
}
