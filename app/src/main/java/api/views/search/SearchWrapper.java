package api.views.search;

import android.app.Activity;
import android.widget.SearchView;

import com.dgos.findfood.findfood.R;

/**
 * Created by dhimil on 7/12/15.
 */
public class SearchWrapper {
    private SearchView searchView;
    private final Activity mainActivity;

    public SearchWrapper(Activity mainActivity) {
        this.mainActivity = mainActivity;
        searchView = (SearchView) this.mainActivity.findViewById(R.id.searchView);
    }

    public SearchWrapper(Activity mainActivity, SearchEvents events) {
        this(mainActivity);
        if (events!= null) {
            SearchListeners listeners = new SearchListeners(events);
            searchView.setOnFocusChangeListener(listeners);
            searchView.setOnQueryTextListener(listeners);
        }
    }

    public void setQueryHint(String queryHint) {
        searchView.setQueryHint(queryHint);
    }
}
