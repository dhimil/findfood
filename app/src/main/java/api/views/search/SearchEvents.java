package api.views.search;

import android.view.View;

/**
 * Created by dhimil on 7/12/15.
 */
public interface SearchEvents {
    public void focusChanged(View view, boolean hasFocus);
    public boolean queryTextSubmit(String query);
    public boolean queryTextChange(String newText);
}
