package api.views.search;


import android.view.View;
import android.widget.SearchView;

/**
 * Created by dhimil on 7/12/15.
 */
public class SearchListeners
        implements View.OnFocusChangeListener, SearchView.OnQueryTextListener {
    SearchEvents events;

    public SearchListeners(SearchEvents events) {
        this.events = events;
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        events.focusChanged(view, hasFocus);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return events.queryTextSubmit(query);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return events.queryTextChange(newText);
    }
}
