package api.data.source;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;

/**
 * Created by dhimil on 7/12/15.
 */
public class SodaDataSource implements DataSource {
    private static final String TAG = SodaDataSource.class.getCanonicalName();
    private static JSONArray object;

    private final String url;

    public SodaDataSource(String url) {
        this.url = url;
    }

    @Override
    public JSONArray queryData() {
        if (object != null) {
            return object;
        }
        HttpClient httpClient= new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String body = EntityUtils.toString(entity);
            object = new JSONArray(body);
            return object;
        } catch (ClientProtocolException e) {
            Log.e(TAG, "ClientProtocolException " + e.getMessage(), e);
        } catch (IOException e) {
            Log.e(TAG, "IOException " + e.getMessage(), e);
        } catch (JSONException e) {
            Log.e(TAG, "IOException " + e.getMessage(), e);
        }
        return null;
    }
}
