package api.data.source;

import org.json.JSONArray;

/**
 * Created by dhimil on 7/13/15.
 */
public interface DataSource {
    public JSONArray queryData();
}
