package com.dgos.findfood.api.consumer;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.model.Marker;

import api.maps.MapEvents;
import api.views.TextView;

/**
 * Created by dhimil on 7/12/15.
 */
public class MapEventListeners implements MapEvents {

    private static final String TAG = MapEventListeners.class.getName();

    private Context context;

    @Override
    public boolean markerClick(Marker marker) {
        Log.i(TAG, "Marker clicked " + marker.getTitle());
        marker.showInfoWindow();
        return true;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        Log.i(TAG, "Info should be got now");
        TextView textView = new TextView(context, marker.getTitle());
        return textView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        Log.i(TAG, "Info contents now");
        return null;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
