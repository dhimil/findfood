package com.dgos.findfood.api.consumer;

import android.util.Log;
import android.view.View;

import api.views.search.SearchEvents;

/**
 * Created by dhimil on 7/12/15.
 */
public class SearchEventListeners implements SearchEvents {

    private static final String TAG = SearchEventListeners.class.getName();

    @Override
    public void focusChanged(View view, boolean hasFocus) {
        Log.i(TAG, "Foucus value changed to " + hasFocus);
    }

    @Override
    public boolean queryTextSubmit(String query) {
        Log.i(TAG, "Submit value for searching " + query);
        return true;
    }

    @Override
    public boolean queryTextChange(String newText) {
        Log.i(TAG, "Query text changed " + newText);
        return true;
    }
}
