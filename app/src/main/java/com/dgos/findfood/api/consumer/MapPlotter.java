package com.dgos.findfood.api.consumer;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import api.data.source.DataSource;
import api.data.source.SodaDataSource;
import api.maps.MapWrapper;

/**
 * Created by dhimil on 7/13/15.
 */
public class MapPlotter implements Runnable {
    private static final String TAG = MapPlotter.class.getCanonicalName();

    private MapWrapper mapWrapper;
    private DataSource dataSource;
    private Activity activity;

    private JSONArray plotData;

    private static final String DATA_URL = "http://data.sfgov.org/resource/rqzj-sfat.json";

    public MapPlotter(Activity activity, MapWrapper mapWrapper) {
        this.activity = activity;
        this.mapWrapper = mapWrapper;
        dataSource = new SodaDataSource(DATA_URL);
    }

    @Override
    public void run() {
        plotData = dataSource.queryData();
        if (plotData == null) {
            return;
        }
        activity.runOnUiThread(new Runnable() {
            public float getColor(String facilitytype) {
                if("truck".equalsIgnoreCase(facilitytype)) {
                    return BitmapDescriptorFactory.HUE_ORANGE;
                } else if ("Push Cart".equalsIgnoreCase(facilitytype)) {
                    return BitmapDescriptorFactory.HUE_YELLOW;
                } else {
                    return BitmapDescriptorFactory.HUE_BLUE;
                }
            }

            public String buildDescription(String foodType, String facilityType) {
                StringBuilder str = new StringBuilder("");
                str.append("Food Served: ")
                        .append(foodType)
                        .append("\n\n")
                        .append("Facility: ")
                        .append(facilityType);
                return str.toString();
            }

            @Override
            public void run() {
                for (int i = 0; i < plotData.length(); i++) {
                    try {
                        JSONObject obj = plotData.getJSONObject(i);
                        String foodType = obj.getString("fooditems");
                        double longitude = Double.parseDouble(obj.getString("longitude"));
                        double latitude = Double.parseDouble(obj.getString("latitude"));
                        String facilityType = obj.getString("facilitytype");
                        float markerColor = getColor(facilityType);
                        String description = buildDescription(foodType, facilityType);
                        Log.i(TAG, "Adding marker " + description);
                        mapWrapper.addMarker(description, latitude, longitude, markerColor);
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage(), e);
                    }
                }
            }
        });
    }

    public void start() {
        try {
            (new Thread(this)).start();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }
}
