package com.dgos.findfood.findfood;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.dgos.findfood.api.consumer.MapEventListeners;
import com.dgos.findfood.api.consumer.MapPlotter;
import com.dgos.findfood.api.consumer.SearchEventListeners;

import api.maps.MapWrapper;
import api.views.search.SearchWrapper;

public class FindFood extends FragmentActivity {

    private MapWrapper mapWrapper;
    private SearchWrapper searchWrapper;
    private MapPlotter plotter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_food);
        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (searchWrapper == null) {
            searchWrapper = new SearchWrapper(this, new SearchEventListeners());
            searchWrapper.setQueryHint("Food around you");
        }
        if (mapWrapper == null) {
            MapEventListeners listeners = new MapEventListeners();
            listeners.setContext(this.getApplicationContext());
            mapWrapper = new MapWrapper(this, listeners);
            mapWrapper.addMarker("Your current dummy location", 37.782, -122.447);
        }

        if(plotter == null) {
            plotter = new MapPlotter(this, mapWrapper);
            plotter.start();
        }
    }
}
